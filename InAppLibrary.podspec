#
# Be sure to run `pod lib lint InAppLibrary.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|

  s.name             = "InAppLibrary"
  s.version          = "1.0.0"
  s.summary          = "A short description of InAppLibrary."
  s.description      = <<-DESC
                       An optional longer description of InAppLibrary
                       * Markdown format.
                       * Don't worry about the indent, we strip it!
                       DESC
  s.homepage         = "https://bitbucket.org/Seinh/inapplibrary"
  s.license          = 'MIT'
  s.author           = { "David Diaz" => "david.diaz.isei@gmail.com" }
  s.source           = { :git => "https://Seinh@bitbucket.org/Seinh/inapplibrary.git", :tag => s.version.to_s }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.resource_bundles =
  {
    'InApp' => ['Pod/Assets/*.png']
  }

  s.source_files        = 'Library/include/*.h'
  s.public_header_files = 'Library/include/*.h'

  s.preserve_paths       = 'Library/libInApp.a'
  s.ios.vendored_library = 'Library/libInApp.a'

  s.library  = 'xml2'
  s.xcconfig = { 'HEADER_SEARCH_PATHS' => '$(SDKROOT)/usr/include/libxml2' }

  s.dependency 'FlurrySDK'
  s.dependency 'JumioMobileSDK', '1.3.1'

  # s.frameworks       = 'UIKit', 'MapKit'
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  # s.screenshots      = "www.example.com/screenshots_1", "www.example.com/screenshots_2"

end
