//
//  InAppViewController.m
//  InAppExample
//
//  Created by David Díaz on 2/7/14.
//  Copyright (c) 2014 Pademobile. All rights reserved.
//

#import "InAppViewController.h"

@interface InAppViewController ()

@end

@implementation InAppViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {}
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self button] setTitle:NSLocalizedString(@"buttonText", @"Perform test transaction") forState:UIControlStateNormal];
    [self.view setAccessibilityLabel:@"first view"];
    [self.button setAccessibilityLabel:@"first view button"];
    [self.logTextView setAccessibilityLabel:@"first view log text view"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonPushed:(id)sender
{
    [[self delegate] performTestTransaction];
}

- (void)transactionWillStart
{
    [[self logTextView] setText:@""];
    [self addText:NSLocalizedString(@"start", @"Transaction will start")];
}

- (void)otpWillBeRequested
{
    [self addText:NSLocalizedString(@"otpRequested", @"OTP will be requested")];
}

- (void)transactionWasCancelled
{
    [self addText:NSLocalizedString(@"cancelled", @"Transaction was cancelled")];
}

- (void)transactionDidFinished
{
    [self addText:NSLocalizedString(@"finished", @"Transaction successfully finished")];
}

- (void)errorWasReceivedWithMessage:(NSString *)message
{
    [self addText:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"error", @"An error message was received:") ,message]];
}

-(void) addText:(NSString *)text
{
    NSString *log = [[self logTextView] text];
    log           = [NSString stringWithFormat:@"%@\n%@", log, text];
    [[self logTextView] setText:log];
}

@end
