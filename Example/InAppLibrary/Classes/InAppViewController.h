//
//  InAppViewController.h
//  InAppExample
//
//  Created by David Díaz on 2/7/14.
//  Copyright (c) 2014 Pademobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InApp.h"

@protocol IAppViewDelegate <NSObject>

-(void) performTestTransaction;

@end

@interface InAppViewController : UIViewController<InAppDelegate>

@property(nonatomic, assign) id<IAppViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton   *button;
@property (weak, nonatomic) IBOutlet UITextView *logTextView;

- (IBAction)buttonPushed:(id)sender;

@end
