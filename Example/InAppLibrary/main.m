//
//  main.m
//  InAppLibrary
//
//  Created by David Diaz on 02/13/2015.
//  Copyright (c) 2014 David Diaz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
