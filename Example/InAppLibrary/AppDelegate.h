//
//  AppDelegate.h
//  InAppExample
//
//  Created by David Díaz on 2/6/14.
//  Copyright (c) 2014 Pademobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InApp.h"
#import "InAppViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, IAppViewDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
