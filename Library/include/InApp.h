//
//  InApp.h
//  InApp
//
//  Created by David Díaz on 2/6/14.
//  Copyright (c) 2014 Pademobile. All rights reserved.
//
#import <Foundation/Foundation.h>

@protocol InAppDelegate <NSObject>

-(void)otpWillBeRequested;
-(void)transactionWillStart;
-(void)transactionWasCancelled;
-(void)transactionDidFinished;
-(void)errorWasReceivedWithMessage:(NSString *)message;

@end

@interface InApp : NSObject

@property(nonatomic)       BOOL              production;
@property(nonatomic, weak) id<InAppDelegate> delegate;

-(void)processTransactionWithAmount:(CGFloat)amount
                       withCurrency:(NSString *)currency
                    withCountryCode:(NSString *)code
                         withUserId:(NSString *)userId
                      andPrivateKey:(NSString *)privateKey;

@end
