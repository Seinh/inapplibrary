# InAppLibrary

[![CI Status](http://img.shields.io/travis/David Diaz/InAppLibrary.svg?style=flat)](https://travis-ci.org/David Diaz/InAppLibrary)
[![Version](https://img.shields.io/cocoapods/v/InAppLibrary.svg?style=flat)](http://cocoadocs.org/docsets/InAppLibrary)
[![License](https://img.shields.io/cocoapods/l/InAppLibrary.svg?style=flat)](http://cocoadocs.org/docsets/InAppLibrary)
[![Platform](https://img.shields.io/cocoapods/p/InAppLibrary.svg?style=flat)](http://cocoadocs.org/docsets/InAppLibrary)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

InAppLibrary is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "InAppLibrary"

## Author

David Diaz, david.diaz.isei@gmail.com

## License

InAppLibrary is available under the MIT license. See the LICENSE file for more info.

